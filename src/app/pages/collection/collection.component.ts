import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Category } from '../../models/category';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent implements OnInit {
  categories: Category[];
  selectedCategory: Category;
  showAll = false;
  emptySpaces = 0;

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private webcomicService: WebcomicDataService
  ) {}

  ngOnInit(): void {
    this.webcomicService.showAll.next(this.showAll);
    this.categories = this.categoryService.getCategories();
    this.determineSpaces();
  }

  selectCategory(category: Category) {
    this.selectedCategory = category;
    if (category.name === 'Favorites') {
      this.webcomicService.searchProperty.next('like');
    } else {
      this.webcomicService.searchProperty.next('category');
    }
    this.router.navigate([
      '/collection/categories',
      this.selectedCategory.name,
    ]);
  }

  showWebcomics() {
    this.showAll = true;
    this.webcomicService.showAll.next(this.showAll);
  }

  showCategories() {
    this.showAll = false;
    this.webcomicService.showAll.next(this.showAll);
  }

  determineSpaces() {
    if (this.categories.length % 3 === 1) {
      this.emptySpaces = 1;
    } else if (this.categories.length % 3 === 2) {
      this.emptySpaces = 2;
    }
  }
}
