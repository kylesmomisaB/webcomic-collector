import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Webcomic } from '../../../models/webcomic';

@Component({
  selector: 'app-webcomic-editor',
  templateUrl: './webcomic-editor.component.html',
  styleUrls: ['./webcomic-editor.component.scss'],
})
export class WebcomicEditorComponent implements OnInit, OnDestroy {
  tags: string[];
  webcomicDataSubscription: Subscription;
  webcomic: Webcomic;
  isNewWebcomic: boolean;
  webcomicForm: FormGroup;
  tagForm: FormControl;
  id: number;

  constructor(
    private webcomicService: WebcomicDataService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.isNewWebcomic = params['id'] == null;
      this.id = params['id'];
    });

    if (!this.isNewWebcomic) {
      this.webcomicDataSubscription = this.webcomicService.selectedWebcomic.subscribe(
        (selectedWebcomic: Webcomic) => {
          this.webcomic = selectedWebcomic;
        }
      );
    } else {
      this.webcomic = new Webcomic('', '', '', '', [], false);
    }
    this.initializeForms();
    this.tags = this.webcomic.tags.slice();
  }

  private initializeForms() {
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.webcomicForm = new FormGroup({
      URL: new FormControl(this.webcomic.imgURL, [
        Validators.required,
        Validators.pattern(reg),
      ]),
      title: new FormControl(this.webcomic.title),
      category: new FormControl(this.webcomic.category, Validators.required),
    });
    this.tagForm = new FormControl('');
  }

  saveWebcomic() {
    this.webcomic.imgURL = this.webcomicForm.value.URL;
    this.webcomic.title = this.webcomicForm.value.title;
    this.webcomic.category = this.webcomicForm.value.category;
    this.webcomic.tags = this.tags;
    if (this.isNewWebcomic) {
      this.webcomicService.addNewWebcomic(this.webcomic);
    } else {
      this.webcomicService.updateWebcomic(this.webcomic, this.id);
    }
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  deleteWebcomic() {
    this.webcomicService.deleteWebcomic(this.id);
    this.router.navigate(['/collection']);
  }

  addTag() {
    this.tags.push(this.tagForm.value);
    this.tagForm.reset();
  }

  deleteTag(index: number) {
    this.tags.splice(index, 1);
  }

  ngOnDestroy() {
    if (this.webcomicDataSubscription) {
      this.webcomicDataSubscription.unsubscribe();
    }
  }
}
