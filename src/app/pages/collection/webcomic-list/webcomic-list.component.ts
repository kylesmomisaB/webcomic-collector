import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Webcomic } from '../../../models/webcomic';
import { Category } from '../../../models/category';

@Component({
  selector: 'app-webcomic-list',
  templateUrl: './webcomic-list.component.html',
  styleUrls: ['./webcomic-list.component.scss'],
})
export class WebcomicListComponent implements OnInit {
  categoryName: string;
  category: Category;
  webcomics: Webcomic[];
  emptySpaces = 0;
  categoryEditable: boolean;
  @Input() showAll: boolean;
  categoryMenuVisible = false;

  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private webcomicSercive: WebcomicDataService
  ) {}

  ngOnInit(): void {
    if (!this.showAll) {
      this.route.params.subscribe((params: Params) => {
        this.categoryName = params['name'];
        this.category = this.categoryService.getCategoryByName(
          this.categoryName
        );
      });
      this.webcomics = this.webcomicSercive.getWebcomics(this.category.name);
    } else {
      this.webcomics = this.webcomicSercive.getAllWebcomics();
    }
    this.determineSpaces();
    console.log(this.categoryMenuVisible);
  }

  visitWebcomicSource() {
    if (
      this.categoryService.categoryExits(this.category.name) &&
      this.category.name !== ('Favorites' || 'misc')
    ) {
      window.open(this.category.URL, '_blank');
    }
  }

  selectWebcomic(webcomic: Webcomic) {
    this.webcomicSercive.onSelectWebcomic(webcomic);
  }

  determineSpaces() {
    if (this.webcomics.length % 3 === 1) {
      this.emptySpaces = 1;
    } else if (this.webcomics.length % 3 === 2) {
      this.emptySpaces = 2;
    }
  }

  toggleCategoryMenu() {
    this.categoryMenuVisible = !this.categoryMenuVisible;
  }

  hideCategoryMenu() {
    this.categoryMenuVisible = false;
    console.log('penis');
  }
}
