import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Webcomic } from 'src/app/models/webcomic';

@Component({
  selector: 'app-webcomic-view',
  templateUrl: './webcomic-view.component.html',
  styleUrls: ['./webcomic-view.component.scss'],
})
export class WebcomicViewComponent implements OnInit, OnDestroy {
  webcomic: Webcomic;
  webcomicDataSubscription: Subscription;
  searchModeSubscription: Subscription;
  webcomicTitle: string;
  categoryImgUrl: string;
  zoom = false;
  isWebcomicView: boolean;
  searchMode: string;

  constructor(
    private webcomicService: WebcomicDataService,
    private categoryService: CategoryService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.webcomicDataSubscription = this.webcomicService.selectedWebcomic.subscribe(
      (selectedWebcomic: Webcomic) => {
        this.webcomic = selectedWebcomic;
      }
    );
    this.webcomicTitle = this.getWebcomicTitle(this.webcomic);
    this.categoryImgUrl = this.getCategoryImgUrl();

    this.webcomicDataSubscription = this.webcomicService.showAll.subscribe(
      (showAll: boolean) => {
        this.isWebcomicView = showAll;
      }
    );

    this.searchModeSubscription = this.webcomicService.searchProperty.subscribe(
      (searchMode: string) => {
        this.searchMode = searchMode;
      }
    );
  }

  showWebcomicCategory() {
    if (this.categoryService.categoryExits(this.webcomic.category)) {
      this.router.navigate(['/collection/categories', this.webcomic.category]);
    }
  }

  nextWebcomic() {
    this.webcomicService.getNextWebcomic(
      this.webcomic,
      this.isWebcomicView,
      this.searchMode
    );
    this.webcomicTitle = this.getWebcomicTitle(this.webcomic);
    this.categoryImgUrl = this.getCategoryImgUrl();
  }

  previousWebcomic() {
    this.webcomicService.getPreviousWebcomic(
      this.webcomic,
      this.isWebcomicView,
      this.searchMode
    );
    this.webcomicTitle = this.getWebcomicTitle(this.webcomic);
    this.categoryImgUrl = this.getCategoryImgUrl();
  }

  getWebcomicTitle(webcomic: Webcomic) {
    let webcomicTitle: string;
    if (webcomic.title.length > 0) {
      webcomicTitle = webcomic.title;
    } else {
      webcomicTitle =
        'No. ' + (this.webcomicService.getWebcomicCategoryPos(webcomic) + 1);
    }
    return webcomicTitle;
  }

  getCategoryImgUrl() {
    let categoryImgUrl: string;
    if (this.categoryService.categoryExits(this.webcomic.category)) {
      categoryImgUrl = this.categoryService.getCategoryByName(
        this.webcomic.category
      ).imgURL;
    } else {
      categoryImgUrl = 'assets/images/misc_2.png';
    }
    return categoryImgUrl;
  }

  zoomImg() {
    this.zoom = !this.zoom;
  }

  like() {
    this.webcomic.like = !this.webcomic.like;
  }

  visitSource() {
    if (this.webcomic.URL.length > 0) {
      window.open(this.webcomic.URL, '_blank');
    } else {
      window.open(this.webcomic.imgURL, '_blank');
    }
  }

  copyToClipboard(str: string) {
    let element = document.createElement('textarea');
    element.value = str;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
  }

  saveURLToClipboard() {
    this.copyToClipboard(this.webcomic.imgURL);
  }

  ngOnDestroy() {
    this.webcomicDataSubscription.unsubscribe();
    this.searchModeSubscription.unsubscribe();
  }
}
