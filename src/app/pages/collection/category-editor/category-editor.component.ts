import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.scss'],
})
export class CategoryEditorComponent implements OnInit {
  categoryForm: FormGroup;
  category: Category;
  isNewCategory: boolean;
  categoryName: string;
  id: number;
  categoryDataSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.isNewCategory = params['name'] == null;
      this.categoryName = params['name'];
      console.log('categoryName');
      console.log(this.categoryName);
      // console.log('isNewCategory');
      // console.log(this.isNewCategory);
      // this.id = params['id'];
    });

    // this.categoryService.getCategoryPosition('xkcd');
    if (this.isNewCategory) {
      this.category = new Category('', '', '', '');
    } else {
      this.category = this.categoryService.getCategoryByName(this.categoryName);
    }

    this.initializeForm();
  }

  private initializeForm() {
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.categoryForm = new FormGroup({
      title: new FormControl(this.category.name, Validators.required),
      URL: new FormControl(this.category.URL, [
        Validators.required,
        Validators.pattern(reg),
      ]),
      imgURL: new FormControl(this.category.imgURL),
      status: new FormControl(this.category.status),
    });
  }
}
