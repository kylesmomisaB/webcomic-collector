import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { WebcomicDataService } from '../../services/webcomic-data.service';
import { Webcomic } from '../../models/webcomic';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  webcomics: Webcomic[];
  fetchedComics: Webcomic[];
  fet: any;

  constructor(
    private http: HttpClient,
    private webcomicSercive: WebcomicDataService
  ) {}

  ngOnInit(): void {
    this.webcomics = this.webcomicSercive.getAllWebcomics();
  }

  show() {
    console.log(this.webcomics);
  }

  onTest() {
    this.http
      .post(
        'https://what-to-eat-99415.firebaseio.com/comics.json',
        this.webcomics
      )
      .subscribe((responseData) => {
        console.log(responseData);
      });
  }
}
