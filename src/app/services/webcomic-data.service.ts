import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CategoryService } from './category.service';
import { Webcomic } from '../models/webcomic';

@Injectable({
  providedIn: 'root',
})
export class WebcomicDataService {
  webcomics: Webcomic[] = [
    new Webcomic(
      'http://www.nataliedee.com/index.php?date=101910',
      'http://www.nataliedee.com/101910/fresh-new-brain.jpg',
      '',
      'Natalie Dee',
      ['brains', 'mean', 'non-PC'],
      false
    ),
    new Webcomic(
      '',
      'http://www.nataliedee.com/110512/i-swear-if-i-get-one-more-email-or-call-asking-for-money-im-voting-for-jill-stein.jpg',
      '',
      'Natalie Dee',
      ['nlanla', 'test', 'more test', 'penis'],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/abdd7e139dec383a8129f283411596f0/e4bfb68467e7e86f-f7/s540x810/b39e4c0c1b8fa5ab3618685e273356f597aaf2ad.jpg',
      '',
      'Sarahs Scribbles',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.asofterworld.com/clean/urban.jpg',
      'Urban',
      'A softer World',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.asofterworld.com/clean/simple.jpg',
      'Simple',
      'A softer World',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/8b0/8b01b0c5f3b7020b5862b7fc9ae2f717.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/7c7/7c71ea7319ae498cb2fa971bad14384c.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/ai_hiring_algorithm.png',
      'AI hiring algorithm',
      'xkcd',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/grownups.png',
      'Grownups',
      'xkcd',
      [],
      true
    ),
    new Webcomic(
      '',
      'http://files.explosm.net/comics/Kris/push.png',
      'push',
      'Cyanide & Happiness',
      [],
      false
    ),
    new Webcomic(
      '',
      'http://www.nataliedee.com/032112/youre-gonna-die-choking-on-a-fortune-cookie-actually.jpg',
      '',
      'Natalie Dee',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/b93f3a3577b8a86e6d43592c027b1212/tumblr_ole5tbTCQw1qiuiebo1_640.jpg',
      '',
      'Sarahs Scribbles',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.asofterworld.com/clean/reconsider.jpg',
      'Reconsider',
      'A softer World',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.asofterworld.com/clean/terracotta.jpg',
      'Terracotta',
      'A softer World',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/5a0/5a0a09db631b3b6b73b40070042d68fe.jpg',
      '',
      'Lingvistov',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/cc9/cc9069c948a5299ab2cc9becb4bfd4e3.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/error_types.png',
      'Error Types',
      'xkcd',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/alive_or_not.png',
      'Alive or not',
      'xkcd',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://files.explosm.net/comics/Rob/family.jpg?t=26A2B4',
      '',
      'Cyanide & Happiness',
      [],
      true
    ),
    new Webcomic(
      '',
      'http://www.nataliedee.com/082911/too-bad-its-not-in-a-good-way.jpg',
      '',
      'Natalie Dee',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/9f44481e790814a1c0a6a7daf5301378/tumblr_ol18zcyqx91qiuiebo1_540.jpg',
      '',
      'Sarahs Scribbles',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://www.asofterworld.com/clean/faith.jpg',
      'Faith',
      'A softer World',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/8ef/8efc294044a27081f9304705be6040d7.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/ba6/ba61d2c6bdf8ac40a07aee2d95a0614e.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/tattoo_ideas.png',
      'Tattoo Ideas',
      'xkcd',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://imgs.xkcd.com/comics/further_research_is_needed.png',
      'Further reserach is needed',
      'xkcd',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://files.explosm.net/comics/Dave/jobcenter.png?t=AE8838',
      'Jobcenter',
      'Cyanide & Happiness',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/f706b89e99be4737d24e9c38514af341/tumblr_oktyfibz901qiuiebo1_540.jpg',
      '',
      'Sarahs Scribbles',
      [],
      true
    ),
    new Webcomic(
      '',
      'http://www.marriedtothesea.com/032906/pollution-time.gif',
      '',
      'Married to the sea',
      [],
      false
    ),
    new Webcomic(
      '',
      'http://www.marriedtothesea.com/012710/stop-hitting-yourself.gif',
      '',
      'Married to the sea',
      [],
      false
    ),
    new Webcomic(
      '',
      'http://www.marriedtothesea.com/030608/ashes-to-ashes-beer-to-pee.gif',
      '',
      'Married to the sea',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/f3b496399346c2e5ebe34296f7b56524/tumblr_olq7jyhEmv1s24ep9o1_1280.png',
      'Social Media Bingo',
      'Gemma Correll',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/2d994fabbcf4d693672e6b79b91c0c9c/tumblr_o8fh3ptIDM1qhlsrfo1_640.jpg',
      '',
      'Gemma Correll',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/f013f6e52ca6b7f98a442f796d775207/tumblr_o5otg8MUrY1qhlsrfo1_640.gifv',
      '',
      'Gemma Correll',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/b362da8e411b50830f41b5a4f2ca2701/tumblr_o0tak0s9B71qbe766o6_1280.jpg',
      '',
      'Gemma Correll',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/72fa26c4cf5dc5d6fb1b253fffe6b749/50b101566e65ce9f-05/s1280x1920/2220df6adbc581ccf5b35cf4f7d5671d22e6a335.jpg',
      '',
      'Gemma Correll',
      [],
      true
    ),
    new Webcomic(
      '',
      'https://www.lingvistov.ru/upload/iblock/8de/8deb39b804f119203470d49aa33cb108.jpg',
      '',
      'Lingvistov',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://64.media.tumblr.com/813654005bfc8babb52eda596d9b0098/tumblr_nwudk3AQG11qhlsrfo1_640.jpg',
      '',
      'Gemma Correll',
      [],
      false
    ),
    new Webcomic(
      '',
      'https://files.explosm.net/comics/Kris/google.png?t=50FD17',
      'Google',
      'Cyanide & Happiness',
      [],
      false
    ),
    new Webcomic(
      '',
      'http://thedoghousediaries.com/dhdcomics/2014-11-10.png',
      '',
      'The Doghouse Diaries',
      [],
      false
    ),
    new Webcomic(
      '',
      'http://thedoghousediaries.com/dhdcomics/2015-06-26.png',
      'Organizational Structures: A Survival Guide',
      'The Doghouse Diaries',
      [],
      false
    ),
  ];

  readonly showAll = new BehaviorSubject<boolean>(false);
  selectedWebcomic = new BehaviorSubject<Webcomic>(this.webcomics[0]);
  searchProperty = new BehaviorSubject<string>('category');

  constructor(
    private router: Router,
    private categoryService: CategoryService
  ) {}

  onSelectWebcomic(webcomic: Webcomic) {
    this.selectedWebcomic.next(webcomic);
    const webcomicId = this.getWebcomicPos(this.webcomics, webcomic) + 1;
    this.router.navigate(['/collection/webcomics', webcomicId]);
  }

  getAllWebcomics() {
    return this.webcomics.slice();
  }

  getWebcomicPos(webcomics: Webcomic[], webcomic: Webcomic) {
    let pos = webcomics
      .map((searchByImgURL) => {
        return searchByImgURL.imgURL;
      })
      .indexOf(webcomic.imgURL);
    return pos;
  }

  getWebcomicsByCategory(category: string) {
    let filteredWebcomics = this.webcomics.filter((webcomic) => {
      return webcomic.category.toLowerCase().includes(category.toLowerCase());
    });
    return filteredWebcomics;
  }

  getMiscWebcomics() {
    let existingCategories = this.categoryService.getCategoryNames();
    let filteredWebcomics = this.webcomics.filter((webcomic) => {
      return !existingCategories.includes(webcomic.category.toLowerCase());
    });
    return filteredWebcomics;
  }

  getFavorites() {
    let filteredWebcomics = this.webcomics.filter((webcomic) => {
      return webcomic.like;
    });
    return filteredWebcomics;
  }

  getWebcomics(category: string) {
    let chosenWebcomics: Webcomic[];
    if (category === 'Favorites') {
      chosenWebcomics = this.getFavorites();
    } else if (category === 'misc') {
      chosenWebcomics = this.getMiscWebcomics();
    } else {
      chosenWebcomics = this.getWebcomicsByCategory(category);
    }
    return chosenWebcomics;
  }

  getWebcomicCategoryPos(webcomic: Webcomic) {
    let filteredWebcomics = this.getWebcomicsByCategory(webcomic.category);
    let pos = this.getWebcomicPos(filteredWebcomics, webcomic);
    return pos;
  }

  addNewWebcomic(webcomic: Webcomic) {
    this.webcomics.push(webcomic);
  }

  updateWebcomic(webcomic: Webcomic, position: number) {
    this.webcomics[position - 1] = webcomic;
  }

  deleteWebcomic(id: number) {
    this.webcomics.splice(id - 1, 1);
  }

  determineNextPosCategoryView(
    previousWebcomic: Webcomic,
    previousPos: number,
    searchProperty: string
  ) {
    let nextPos: number;
    if (searchProperty === 'category') {
      nextPos = this.webcomics
        .map((searchByCategory) => {
          return searchByCategory.category.toLowerCase();
        })
        .indexOf(previousWebcomic.category.toLowerCase(), previousPos + 1);
      if (nextPos === -1) {
        nextPos = this.webcomics
          .map((searchByCategory) => {
            return searchByCategory.category.toLowerCase();
          })
          .indexOf(previousWebcomic.category.toLowerCase());
      }
    } else if (searchProperty === 'like') {
      nextPos = this.webcomics
        .map((searchByCategory) => {
          return searchByCategory.like;
        })
        .indexOf(previousWebcomic.like, previousPos + 1);
      if (nextPos === -1) {
        nextPos = this.webcomics
          .map((searchByCategory) => {
            return searchByCategory.like;
          })
          .indexOf(previousWebcomic.like);
      }
    }
    return nextPos;
  }

  determineNextPosWebcomicView(previousPos: number) {
    let nextPos: number;
    if (previousPos < this.webcomics.length - 1) {
      nextPos = previousPos + 1;
    } else {
      nextPos = 0;
    }
    return nextPos;
  }

  getNextWebcomic(
    previousWebcomic: Webcomic,
    showWebcomics: boolean,
    searchProperty: string
  ) {
    let previousPos = this.getWebcomicPos(this.webcomics, previousWebcomic);
    let nextPos: number;
    if (!showWebcomics) {
      nextPos = this.determineNextPosCategoryView(
        previousWebcomic,
        previousPos,
        searchProperty
      );
    } else {
      nextPos = this.determineNextPosWebcomicView(previousPos);
    }
    this.selectedWebcomic.next(this.webcomics[nextPos]);
    this.router.navigate(['/collection/webcomics', nextPos + 1]);
  }

  determinePreviousPosCategoryView(
    previousWebcomic: Webcomic,
    searchProperty: string
  ) {
    let filteredWebcomics: Webcomic[];
    if (searchProperty === 'like') {
      filteredWebcomics = this.getWebcomics('Favorites');
    } else {
      filteredWebcomics = this.getWebcomics(previousWebcomic.category);
    }
    let pos = this.getWebcomicPos(filteredWebcomics, previousWebcomic);
    let nextWebcomic: Webcomic;
    let nextWebcomicPos: number;
    if (pos != 0) {
      nextWebcomic = filteredWebcomics[pos - 1];
    } else {
      nextWebcomic = filteredWebcomics[filteredWebcomics.length - 1];
    }
    nextWebcomicPos = this.getWebcomicPos(this.webcomics, nextWebcomic);
    return nextWebcomicPos;
  }

  determinePreviousPosWebcomicView(previousPos: number) {
    let nextPos: number;
    if (previousPos != 0) {
      nextPos = previousPos - 1;
    } else {
      nextPos = this.webcomics.length - 1;
    }
    return nextPos;
  }

  getPreviousWebcomic(
    previousWebcomic: Webcomic,
    showWebcomics: boolean,
    searchProperty: string
  ) {
    let previousPos = this.getWebcomicPos(this.webcomics, previousWebcomic);
    let nextPos: number;
    if (!showWebcomics) {
      nextPos = this.determinePreviousPosCategoryView(
        previousWebcomic,
        searchProperty
      );
    } else {
      nextPos = this.determinePreviousPosWebcomicView(previousPos);
    }
    this.selectedWebcomic.next(this.webcomics[nextPos]);
    this.router.navigate(['/collection/webcomics', nextPos + 1]);
  }
}
