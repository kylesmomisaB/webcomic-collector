import { Injectable } from '@angular/core';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  categories: Category[] = [
    new Category('Favorites', '', 'assets/images/favorites.png', 'none'),
    new Category('misc', '', 'assets/images/misc_2.png', 'none'),
    new Category(
      'the system',
      'https://www.systemcomic.com/',
      'http://www.systemcomic.com/comics/2013/08/murderdog-300x245.png',
      'active'
    ),
    new Category(
      'xkcd',
      'https://xkcd.com/',
      'https://what-if.xkcd.com/imgs/a/151/insect.png',
      'active'
    ),
    new Category(
      'Sarahs Scribbles',
      'https://sarahcandersen.com/',
      'https://i.pinimg.com/originals/bb/73/55/bb73552880ad313156d8cf4a6ae24f4c.png',
      'active'
    ),
    new Category(
      'Natalie Dee',
      'http://www.nataliedee.com/index.php',
      'http://www.nataliedee.com/111908/whatever-dude-whatever.jpg',
      'discontinued'
    ),
    new Category(
      'A Softer World',
      'https://asofterworld.com/',
      'https://www.asofterworld.com/clean/end.jpg',
      'discontinued'
    ),
    new Category(
      'Cyanide & Happiness',
      'https://explosm.net/',
      'https://rcg-cdn.explosm.net/panels/1DEB9C6671B5.png',
      'active'
    ),
    new Category(
      'Lingvistov',
      'https://www.lingvistov.ru/doodles/?PAGEN_1=2&SIZEN_1=100',
      'https://www.lingvistov.ru/upload/iblock/f50/f5048a4e8c69aeae6dc1566b4dc5072a.jpg',
      'active'
    ),
    new Category(
      'toothpaste for dinner',
      'http://www.toothpastefordinner.com/index.php',
      'http://toothpastefordinner.com/060514/odorant.gif',
      'active'
    ),
    new Category(
      'married to the sea',
      'http://www.marriedtothesea.com/index.php',
      'http://marriedtothesea.com/111115/great-news-everyone.gif',
      'active'
    ),
    new Category(
      'Gemma Correll',
      'https://gemmacorrell.tumblr.com/',
      'https://cdn.shopify.com/s/files/1/2698/6032/files/Gemma-Twitter-584px_1024x1024_3ecae781-4512-4d41-945f-e6edc106b6dd.png?v=1518458654',
      'active'
    ),
    new Category(
      'rubyetc',
      'https://rubyetc.tumblr.com/',
      'https://64.media.tumblr.com/7039f488696f7509fa58987b069e2ab7/tumblr_pcvqbh5Mxb1qglhc3o1_500.jpg',
      'discontinued'
    ),
  ];

  constructor() {}

  getCategories() {
    return this.categories.slice();
  }

  getCategoryByName(name: string) {
    let pos = this.categories
      .map((searchByName) => {
        return searchByName.name.toLowerCase();
      })
      .indexOf(name.toLowerCase());
    return this.categories.slice()[pos];
  }

  getCategoryPosition(name: string) {
    let pos = this.categories
      .map((searchByName) => {
        return searchByName.name.toLowerCase();
      })
      .indexOf(name.toLowerCase());
    console.log('pos)');
    console.log(pos);
    return pos;
  }

  getCategoryNames() {
    let categoryNames = [];
    let editableCategories = this.categories.slice(2);
    editableCategories.forEach((category) => {
      categoryNames.push(Object.values(category)[0].toLowerCase());
    });
    return categoryNames;
  }

  categoryExits(categoryName: string) {
    let filteredCategories: Category[];
    filteredCategories = this.categories.filter((category) => {
      return category.name.toLowerCase().includes(categoryName.toLowerCase());
    });
    return filteredCategories.length > 0;
  }

  addNewCategory(category: Category) {
    this.categories.push(category);
  }

  // updateCategory(category: Category, position: number) {
  //   this.categories[position - 1] = webcomic;
  // }

  // deleteCategory(id: number) {
  //   this.webcomics.splice(id - 1, 1);
  // }
}
