export class Webcomic {
  public URL: string;
  public imgURL: string;
  public title: string;
  public category: string;
  public tags: string[];
  public like: boolean;

  constructor(
    URL: string,
    imgURL: string,
    title: string,
    category: string,
    tags: string[],
    like: boolean
  ) {
    this.URL = URL;
    this.imgURL = imgURL;
    this.title = title;
    this.category = category;
    this.tags = tags;
    this.like = like;
  }
}
