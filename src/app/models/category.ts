export class Category {
  public name: string;
  public URL: string;
  public imgURL: string;
  public status: string;

  constructor(name: string, URL: string, imgURL: string, status: string) {
    this.name = name;
    this.URL = URL;
    this.imgURL = imgURL;
    this.status = status;
  }
}
