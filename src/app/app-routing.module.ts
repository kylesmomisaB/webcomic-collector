import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { CategoryEditorComponent } from './pages/collection/category-editor/category-editor.component';
import { CollectionComponent } from './pages/collection/collection.component';
import { WebcomicEditorComponent } from './pages/collection/webcomic-editor/webcomic-editor.component';
import { WebcomicListComponent } from './pages/collection/webcomic-list/webcomic-list.component';
import { WebcomicViewComponent } from './pages/collection/webcomic-list/webcomic-view/webcomic-view.component';

const routes: Routes = [
  { path: '', redirectTo: '/collection', pathMatch: 'full' },
  {
    path: 'collection',
    children: [
      { path: '', component: CollectionComponent },
      { path: 'new', component: WebcomicEditorComponent },
      { path: 'new-category', component: CategoryEditorComponent },
      { path: 'categories/:name', component: WebcomicListComponent },
      { path: 'categories/:name/edit', component: CategoryEditorComponent },
      { path: 'webcomics/:id', component: WebcomicViewComponent },
      { path: 'webcomics/:id/edit', component: WebcomicEditorComponent },
    ],
  },
  { path: 'about', component: AboutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
