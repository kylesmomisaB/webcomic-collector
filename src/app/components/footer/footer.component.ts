import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  mobileSearchVisible = false;

  constructor() {}

  ngOnInit(): void {}

  toggleMobileSearch() {
    this.mobileSearchVisible = !this.mobileSearchVisible;
  }
}
