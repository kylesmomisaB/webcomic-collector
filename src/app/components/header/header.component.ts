import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('burger') burger: ElementRef;
  mobileMenuVisible = false;
  animationDisabled = true;
  mobileSearchVisible = false;

  constructor() {}

  ngOnInit(): void {}

  disableAnimation() {
    if (this.animationDisabled == true) {
      this.animationDisabled = false;
    }
  }

  toggleMobileMenu() {
    this.burger.nativeElement.classList.toggle('open');
    this.mobileMenuVisible = !this.mobileMenuVisible;
  }

  toggleMobileSearch() {
    this.mobileSearchVisible = !this.mobileSearchVisible;
    if (this.mobileMenuVisible) {
      this.toggleMobileMenu();
    }
  }
}
