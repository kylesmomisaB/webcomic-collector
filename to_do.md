# To do list

### Urgent
* Add category editor and option to add new categories
* Move webcomic and category data to server
* Implement desktop (and tablet) version 
* Write about text
* Add functionality to search bar
* Implement back button



### Future Improvements
* Create scraper for webcomic image URL and  webcomic title from webcomic webpage URL
* Extend share button functionality

### Other
* Make better pictures for misc and favorites
